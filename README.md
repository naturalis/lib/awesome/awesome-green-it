# awesome-green-it

[![Awesome](https://awesome.re/badge-flat2.svg)](https://awesome.re)

This is a awesome list of initiatives, projects, software and anything else related to the broad topic of Green IT.

[What is an awesome list?](https://github.com/sindresorhus/awesome/blob/main/awesome.md)

## Organizations

- [Boavizta](https://boavizta.org)
- [Climate Action.tech](https://climateaction.tech)
- [Digital Cleanup Day](https://www.digitalcleanupday.org/)
- [Green Software Foundation](https://greensoftware.foundation)
- [Green Web Foundation](https://thegreenwebfoundation.org)
- [Kennisplatform Green IT Digitaal Erfgoed](http://kia.pleio.nl/groups)
- [Green Software Practitioner](https://learn.greensoftware.foundation/)
- [CNCF TAG Environmental Sustainability](https://tag-env-sustainability.cncf.io/)

## Sustainability policies

- [SURF - duurzaamheid en MVO](https://surf.nl/ict-voorzieningen/duurzaamheid-en-mvo)
- [Denk doe duurzaam](https://denkdoeduurzaam.nl/themas/ict)

## Presentations and videos

- [FOSDEM 2023 Energy track](https://fosdem.org/2023/schedule/track/energy)
- [FOSDEM 2024 Energy track](https://fosdem.org/2024/schedule/track/energy)
- [Leersnacks](https://it-academieoverheid.nl/onderwerpen/m/microlearnings-duurzaamheid) 

## Podcasts

- [Environment Variables](https://podcast.greensoftware.foundation)
- [The Energy Transition Show](https://xenetwork.org/ets)


## Articles

- [Green Code: The Secrets to Unlocking More Sustainable Software Engineering](https://www.contino.io/insights/green-coding)

## Training / Certification

- [The Principles of Sustainable Software Engineering](https://learn.microsoft.com/en-us/training/modules/sustainable-software-engineering-overview/)
- [Green Software for Practitioners (LFC131)](https://training.linuxfoundation.org/training/green-software-for-practitioners-lfc131/)

## Software / API's

- [BoaviztAPI](https://doc.api.boavizta.org/)
- [CO2 Signal](https://co2signal.com)
- [Carbon Intensity Aware Scheduling](https://sustainable-computing.io)
- [Cloud Carbon Footprint](https://cloudcarbonfootprint.org)
- [Electricity Maps](https://electricitymaps.com)
- [ENTSO-E](https://transparency.entsoe.eu/)
- [Green Metrics Tool](https://www.green-coding.berlin/projects/green-metrics-tool/)
- [Home Assistant](https://home-assistant.io/home-energy-management)
- [Linux Foundation Energy](https://lfenergy.org)
- [Scaphandre](https://github.com/hubblo-org/scaphandre)
- [kube-green](https://kube-green.dev/)
- [Kepler](https://sustainable-computing.io/)

## Online tools to measure impact

- [How sustainable is your digital lifestyle: e-missions.nl](https://e-missions.nl)
- [Website Carbon Calculator](https://www.websitecarbon.com/)
